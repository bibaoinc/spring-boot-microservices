package com.bibao.boot.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.bibao.boot.model.TaxRate;
import com.bibao.boot.model.TaxResult;
import com.bibao.boot.proxy.TaxRateServiceProxy;
import com.bibao.boot.service.TaxService;

@RestController
public class TaxRestService {
	private static final String TAXRATE_SERVICE_URL = "http://localhost:8000/taxrate-service/rest/filingtype/";
	@Autowired
	private TaxService service;
	
	@Autowired
	private TaxRateServiceProxy proxy;
	
	@GetMapping("/filingtype/{filingType}/income/{income}")
	public TaxResult getTaxResult(@PathVariable("filingType") String filingType, @PathVariable("income") double income) {
		String url = TAXRATE_SERVICE_URL + filingType;
		RestTemplate template = new RestTemplate();
		TaxRate taxRate = template.getForObject(url, TaxRate.class);
		TaxResult result = service.calculateTax(taxRate, income);
		result.setPort(taxRate.getPort());
		return result;
	}
	
	@GetMapping("/feign/filingtype/{filingType}/income/{income}")
	public TaxResult getTaxResultByFeign(@PathVariable("filingType") String filingType, @PathVariable("income") double income) {
		TaxRate taxRate = proxy.retrieveTaxRate(filingType);
		TaxResult result = service.calculateTax(taxRate, income);
		result.setPort(taxRate.getPort());
		return result;
	}
}
