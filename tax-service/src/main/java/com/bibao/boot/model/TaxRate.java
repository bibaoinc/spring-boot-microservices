package com.bibao.boot.model;

import java.util.Map;

public class TaxRate {
	private Map<Integer, Double> rate;
	private FilingType filingType;
	private String port;
	
	public Map<Integer, Double> getRate() {
		return rate;
	}
	public void setRate(Map<Integer, Double> rate) {
		this.rate = rate;
	}
	public FilingType getFilingType() {
		return filingType;
	}
	public void setFilingType(FilingType filingType) {
		this.filingType = filingType;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	
}
