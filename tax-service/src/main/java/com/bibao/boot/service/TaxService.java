package com.bibao.boot.service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.bibao.boot.model.TaxRate;
import com.bibao.boot.model.TaxResult;

@Service
public class TaxService {
	public TaxResult calculateTax(TaxRate taxRate, double income) {
		TaxResult result = new TaxResult();
		result.setIncome(income);
		List<Map.Entry<Integer, Double>> levels = taxRate.getRate().entrySet().stream()
													.sorted((a, b) -> a.getKey().compareTo(b.getKey()))
													.collect(Collectors.toList());
		double tax = 0;
		int n = levels.size();
		for (int i=0; i<n-1; i++) {
			if (income<=levels.get(i+1).getKey()) {
				tax += (income - levels.get(i).getKey()) * levels.get(i).getValue();
				break;
			} else {
				tax += (levels.get(i+1).getKey() - levels.get(i).getKey()) * levels.get(i).getValue();
			}
		}
		if (income>levels.get(n-1).getKey()) {
			tax += (income - levels.get(n-1).getKey()) * levels.get(n-1).getValue();
		}
		result.setTax(tax);
		return result;
	}
}
