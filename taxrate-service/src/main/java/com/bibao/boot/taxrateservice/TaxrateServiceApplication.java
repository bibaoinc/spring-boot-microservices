package com.bibao.boot.taxrateservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages="com.bibao.boot")
public class TaxrateServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TaxrateServiceApplication.class, args);
	}

}

